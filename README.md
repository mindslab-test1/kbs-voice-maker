# KBS 음성제작툴

## Framwork
    - Frontend: Vue
    - Backend: Spring boot

## 변경사항
    - Fronend
        - 로그인 페이지 추가
        - 회원가입 페이지 추가
        - 회원가입한 계정 가입승인(mater계정만 접근) 페이지 추가
        - 일기예보 가져오는 버튼 추가
        - 각 계정별로 작업저장 할 수 있도록 수정
        - 로그 페이지 추가, 텍스트 상자에 글자 수 제한

    - Backend
        - user, log 테이블 추가
        - login관련된 rest api 추가 
        - log 관련된 rest api 추가
        - 기상청 open api로 일기예보 스크립트 생상하는 rest api 추가
        - 다운로드 기능 추가
	        - 모든 음성을 하나의 wav 파일로 다운로드 가능
	        - 각 줄마다 개별 wav 파일을 생성하여 zip파일을 다운로드 가능
        - 보내기 기능 추가
	        - kbs dmz서버로 생성된 음성 파일을 전송