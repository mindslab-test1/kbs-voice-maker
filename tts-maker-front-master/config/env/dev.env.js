module.exports = {
    NODE_ENV: 'development',
    FAKE_DATA: false,
    // API_BASE_URL: 'https://tts.tobegin.net'
    API_BASE_URL: 'https://kbs-tool.maum.ai'    
    // API_BASE_URL: 'http://127.0.0.1:8888'    
    // API_BASE_URL: 'http://15.165.16.245:8888'
    // API_BASE_URL: 'http://114.108.173.114:8888'
    //API_BASE_URL: 'http://10.53.41.101:8888'
};
