import Vue from 'vue';
import Router from 'vue-router';

import NotFoundComponent from '@/components/pages/NotFoundComponent.vue';
import store from '../store/index';

Vue.use(Router);

const rejectAuthUser = (to, from, next) => {
    if(store.state.isLogin === true){
        //이미 로그인 된 유저
        alert('이미 로그인을 하였습니다')
        next('Make')
    }else{
        next()
    }
}

const onlyAuthUser = (to, from, next) => {
    if(store.state.isLogin === false){
        //아직 로그인이 안된 유저
        alert('로그인이 필요한 기능입니다.')
        next('/')
    }else{
        next()
    }
}

const onlyMasterUser = (to, from, next) =>{
    if(store.state.isLogin === true && store.state.isMaster === true){
        next()
    }else{
        alert("마스터계정만 접근 가능합니다.")
        next('Make')
    }
}

const routes = [
    // {
    //    path: '/',
    //    redirect: '/Make'
    // },
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        name: 'loginView',
        beforeEnter: rejectAuthUser,
        component: require('@/views/Login').default,
        meta: {
            title: 'login page',
        }
    },
    {
        path: '/signUp',
        name: 'signUpView',
        component: require('@/views/signUpView').default,
        meta: {
            title: '회원가입'
        }
    },
    {
        path: '/log',
        name: 'LogTableView',
        beforeEnter: onlyAuthUser,
        component: require('@/views/LogTableView').default,
        meta: {
            title: '로그'
        }
    },
    {
        path: '/master',
        name: 'masterView',
        beforeEnter: onlyMasterUser,
        component: require('@/views/masterView').default,
        meta: {
            title: '마스터'
        }
    },
    {
       path: '/Make',
       name: 'MakeHomeView',
       beforeEnter: onlyAuthUser,
       component: require('@/views/MakeHomeView').default,
       meta: {
           title: 'TTS 에디터',
        }
    },
    {
        path: '/test',
        name: 'TestHomeView',
        component: require('@/views/TestView').default,
    },
    {
        path: '/test2',
        name: 'TestHomeView2',
        component: require('@/views/TestView2').default,
    },
    { 
       path: '*', 
       component: NotFoundComponent, // Not Found Router Info
       meta: {
           title: 'Not Found',
        }
    }
];
const router = new Router({
    routes
});

const DEFAULT_TITLE = 'TTS Home';
router.afterEach((to, from) => {
    document.title = to.meta.title || DEFAULT_TITLE;
});

export default router;