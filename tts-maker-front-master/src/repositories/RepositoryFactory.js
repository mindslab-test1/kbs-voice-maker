import JobRepository from "./jobRepository";
import SpeakerRepository from './speakerRepository';
import TTSRepository from './ttsRepository';
import DMZRepository from './dmzRepository';

const repositories = {
    job: JobRepository,
    speaker: SpeakerRepository,
    tts: TTSRepository,
    dmz: DMZRepository,
    // other repositories ...
  };
  
  export const RepositoryFactory = {
    get: name => repositories[name]
  };
  