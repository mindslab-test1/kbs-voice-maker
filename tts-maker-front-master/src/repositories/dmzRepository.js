import 'url-search-params-polyfill';
import Repository from "./Repository";

const resource = "/dmz";


export default {
    getUserCategoryList(dmzUserId){
        return Repository.post(`${resource}/userCategoryList`, dmzUserId, {timeout:5000});
    }
};