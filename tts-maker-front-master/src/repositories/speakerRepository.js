import Repository from "./Repository";

const resource = "/speaker";
export default {
    list() {
        return Repository.get(`${resource}`);
    },
    defaultSpeaker(jobId) {
        return Repository.get(`${resource}/randomSpeaker`);
    }
};