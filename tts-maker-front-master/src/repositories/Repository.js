import axios from "axios";

const baseURL = process.env.API_BASE_URL;

const instance = axios.create({
  baseURL
});

instance.defaults.timeout = 600000;

export default instance; 