const updateCursor = ({ commit }, cursor) => {
    return commit('updateCursor', cursor);
};

const clearCursor = ({commit}) => {
    return commit('clearCursor');
};

/**
 * 스크립트 재생 변경 처리 - Action callee
 * - views/edit/EditScriptListItem.vue 사용
 * @param {*} cursor 재생하고자하는 script index 
 */
const forceChangeTrack = ({ commit }, payload) => {
    return commit('forceChangeTrack', payload);
};
const clearChangeTrack = ({ commit }) => {
    return commit('clearChangeTrack');
};

/**
 * 스크립트 재생중 새로작업, 새로불러오기 등 시 강제 정지 처리 - Action calle
 * - views/edit/EditJobButton.vue 사용
 */
const forceTrackStop = ({commit}) => {
    return commit('setForceTrackStop', true);
}
const clearTrackStop = ({commit}) => {
    return commit('setForceTrackStop', false);
};

export default {
    updateCursor,
    clearCursor, 
    forceChangeTrack,
    clearChangeTrack,
    forceTrackStop,
    clearTrackStop,
};