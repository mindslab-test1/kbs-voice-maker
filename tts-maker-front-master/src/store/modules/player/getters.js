const getCursor = ( state ) => {
    return state.cursor;
};

export default {
    getCursor,
};