/**
 * Speaker List 저장
 * @param {*} speakers 
 */
const setSpeakers = (state, speakers) => {
    state.speakers = speakers || [];
};

const setSelectedSpeakerIndex = ( state, index) => {
    state.selectedSpeakerIndex = index;
};

const clearSelectedSpeakerIndex = ( state ) => {
    state.selectedSpeakerIndex = -1;
};

export default {
    setSpeakers,
    
    setSelectedSpeakerIndex,
    clearSelectedSpeakerIndex,
};