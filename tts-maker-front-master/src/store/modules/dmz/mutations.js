
/**
 * User Category List 추가
 * @param {*} setUserCategoryList 
 */
const setUserCategoryList = (state, userCategoryList) => {
    state.userCategoryList = userCategoryList;
}
/**
 * setJobKind 추가
 * @param {*} setJobKind 
 */
const setJobKind = (state, jobKind) => {
    state.jobKind = jobKind;
}


/**
 * setSelectedCategory 추가
 * @param {*} setSelectedCategory 
 */
const setSelectedCategory = (state, selectedCategory) => {
    state.selectedCategory = selectedCategory;
}

/**
 * setSelectedCategoryId 추가
 * @param {*} setSelectedCategoryId 
 */
const setSelectedCategoryId = (state, selectedCategoryId) => {
    state.selectedCategoryId = selectedCategoryId;
}

/**
 * setSelectedChannel 추가
 * @param {*} setSelectedChannel 
 */
const setSelectedChannel = (state, selectedChannel) => {
    state.selectedChannel = selectedChannel;
}

/**
 * setSelectedDate 추가
 * @param {*} setSelectedDate 
 */
const setSelectedDate = (state, selectedDate) => {
    state.selectedDate = selectedDate;
}

/**
 * setSelectedTitle 추가
 * @param {*} setSelectedTitle 
 */
const setSelectedTitle = (state, selectedTitle) => {
    state.selectedTitle = selectedTitle;
}

/**
 * setItemCode 추가
 * @param {*} setItemCode 
 */
const setItemCode = (state, itemCode) => {
    state.itemCode = itemCode;
}

const clearItemCode = (state) => {
    state.itemCode = '';
};

export default {
    setUserCategoryList,
    setJobKind,
    setSelectedCategory,
    setSelectedCategoryId,
    setSelectedChannel,
    setSelectedDate,
    setSelectedTitle,
    setItemCode,
    clearItemCode,
}