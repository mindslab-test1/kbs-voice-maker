import actions from './actions';
import mutations from './mutations';

const state = {
    userCategoryList: [],
    jobKind: 'PG',
    selectedCategory: '',
    selectedChannel: '',
    selectedDate: '',
    selectedTitle: '',
    itemCode: '',
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}