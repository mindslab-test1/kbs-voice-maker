import Vue from 'vue';
import Vuex from 'vuex';

import speaker from './modules/speaker';
import player from './modules/player';
import job from './modules/job';
import dmz from './modules/dmz';
import axios from 'axios';
import VueRouter from 'vue-router';
import router from '../routes/index';
import Repository from '../repositories/Repository'

Vue.use(Vuex);

export default new Vuex.Store({

    state: {
        userInfo: null,
        isLogin: false,
        isMaster: false,
        loginUser: null
    },
    mutations: {
        loginSuccess(state, payload){
            state.userInfo = payload
            state.isLogin= true
        },
        loginError(state){
            state.isLogin=false
        },
        logout(state){
            state.isLogin=false
            state.userInfo=null
        },
        masterUser(state){
            state.isMaster=true
        },
        notMasterUset(state){
            state.isMaster=false
        }
    },
    actions: {
        //로그인 시도
        login({state, commit}, loginObj){
            const API_BASE_URL = process.env.API_BASE_URL;
            const getURL = [ API_BASE_URL, '/login/userCheck' ].join('');

            axios.post(getURL, {
                userId : loginObj.email,
                userPw : loginObj.password
            }).then( (res) => {

                if(res.data === ""){
                    alert("비밀번호 또는 아이디를 다시 확인해주세요.")
                }else if(res.data.confirm === 0 || res.data.auth === 0){
                    alert("승인되지 않은 사용자입니다.")
                }else{

                    (res.data.auth===1)?commit("masterUser"):commit("notMasterUset")
                    const payload_val  = "success"
                    this.loginSuccess=true
                    commit("loginSuccess", payload_val)
                    this.state.loginUser = loginObj.email
                    router.push({name : "MakeHomeView"}) 
                }
            });


        },
        logout({commit}){
            alert("로그아웃 되었습니다.")
            commit("logout")
            router.push({name: "loginView"})
        }
    },

    linkActiveClass: 'active',
    mode: 'hash',
    modules: {
        speaker,
        player,
        job,
        dmz,
    },
    strict: process.env.NODE_ENV !== 'production'
});