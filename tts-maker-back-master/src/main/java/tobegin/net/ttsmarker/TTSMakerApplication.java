package tobegin.net.ttsmarker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TTSMakerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TTSMakerApplication.class, args);
	}

}
