package tobegin.net.ttsmarker.api.provider.maumai.tts.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;
import tobegin.net.ttsmarker.api.provider.maumai.tts.service.TTSApiService;
import tobegin.net.ttsmarker.api.provider.maumai.tts.vo.MaumAIResponseFile;

@RestController
@RequestMapping("/maumAi/tts")
public class TTSApiRestController {
	
	@Autowired
	private TTSApiService maumAiApiService;
		
	@ResponseBody
	@PostMapping(value = "/requestMakeFile/{voiceName}")
	public Mono<MaumAIResponseFile> requestMakeFile(@PathVariable String voiceName, @RequestParam String text) throws IOException, InterruptedException {
		return maumAiApiService.requestTTSMakeFile(voiceName, text);
	}
}
