package tobegin.net.ttsmarker.api.script.service;

import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import reactor.core.publisher.Mono;
import tobegin.net.ttsmarker.api.provider.maumai.tts.service.TTSApiService;
import tobegin.net.ttsmarker.api.provider.maumai.tts.vo.MaumAIResponseFile;
import tobegin.net.ttsmarker.api.script.domain.dto.response.TTSMakeResultVo;

@Service
public class TTSMakeServiceImpl implements TTSMakeService {
	
	@Autowired
	private TTSApiService maumAiApiService;
	
	public Mono<TTSMakeResultVo> requestTTSMakeFile(String voiceName, String text, String dummyFileName) {
		if(StringUtils.hasText(dummyFileName)) {
			return fakeRequestTTSMakeFile(voiceName, text, dummyFileName);
		}
		Mono<MaumAIResponseFile> result = maumAiApiService.requestTTSMakeFile(voiceName, text);
		Mono<TTSMakeResultVo> convertResult = result.map(data -> {
			return TTSMakeResultVo.of(voiceName, text, data);
		});
		
		return convertResult;
	}
	
	public Mono<TTSMakeResultVo> fakeRequestTTSMakeFile(String voiceName, String text, String dummyFileName) {
		TTSMakeResultVo result = TTSMakeResultVo.builder()
			.requestId(UUID.randomUUID().toString())
			.voiceName(voiceName)
			.text(text)
			.statusCd(200)
			.statusNm("정상")
			.downloadURL(String.format("/static/voice/%s", dummyFileName))
			.expiryDate("2019-12-31")
			.build();
		
		Random rnd = new Random();
		int sleep = rnd.nextInt(30) * 100;
		try {
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return Mono.just(result);
	}
}
