package tobegin.net.ttsmarker.api.job.vo.request;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import tobegin.net.ttsmarker.api.job.vo.response.DmzDestVo;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobContentSaveVo {
	private String jobTitle;								// 작업 제목
	private List<JobSpeakerSaveVo> speakers;			// 선택한 화자 리스트
	private List<JobSpeakerSaveVo> deletedSpeakers;	// 삭제한 화자 리스트(DB 저장 데이터 한해)
	private List<JobScriptSaveVo> scripts;				// 작성한 스크립트 리스트
	private List<JobScriptSaveVo> deletedScripts;		// 삭제할 스크립트 리스트
	private String userId;
	private DmzDestVo destination;
}
