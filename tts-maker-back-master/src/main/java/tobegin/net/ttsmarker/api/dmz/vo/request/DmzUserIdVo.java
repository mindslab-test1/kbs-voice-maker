package tobegin.net.ttsmarker.api.dmz.vo.request;

import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DmzUserIdVo {
    private String userId;
}
