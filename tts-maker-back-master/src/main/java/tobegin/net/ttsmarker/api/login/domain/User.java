package tobegin.net.ttsmarker.api.login.domain;


import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.checkerframework.checker.units.qual.C;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "USER", uniqueConstraints = {@UniqueConstraint(name = "UK_USER_ID", columnNames = { "USER_ID" }) })
public class User {

    //관리자번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private String id;

    //user ID
    @Column(name ="USER_ID", columnDefinition = "varchar(100)")
    private String userId;

    //user PW
    @Column(name ="USER_PW", columnDefinition = "varchar(100)")
    private String userPw;

    //권한
    @Column(name="AUTH", columnDefinition = "int(11)")
    private int auth;

    //마스터 계정 승인
    @Column(name="CONFIRM", columnDefinition = "int(11)")
    private int confirm;

    //이름
    @Column(name="NAME", columnDefinition = "varchar(100)")
    private String name;

    //비즈니스 코드
    @Column(name="CODE", columnDefinition = "varchar(100)")
    private String code;
}
