package tobegin.net.ttsmarker.api.job.vo.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import tobegin.net.ttsmarker.api.job.domain.JobScript;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobSettingsVo {
	private String style;
	private Integer volume;
	private String speed;
	private Long sleep;

	public static JobSettingsVo of(JobScript entity) {
        return JobSettingsVo.builder()
        		.style(entity.getSettingsStyle())
        		.volume(entity.getSettingsVolume())
        		.speed(entity.getSettingsSpeed())
        		.sleep(entity.getSettingsSleep())
        		.build();
    }
}
