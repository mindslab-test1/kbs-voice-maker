package tobegin.net.ttsmarker.api.provider.maumai.tts.vo;

import lombok.Data;

@Data
public class MaumAIResponseFile {
	private MaumAIResponseMessage message = new MaumAIResponseMessage();
	private String downloadURL;		// 파일을 다운로드 받을 수 있는 경로
	private String expiryDate; 		// 파일 다운로드 만료일자 (yyyy-MM-dd 형태로 출력)
}
