package tobegin.net.ttsmarker.api.job.vo.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import tobegin.net.ttsmarker.api.job.domain.JobM;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobContentVo {
	private long id;
	private String jobId;
	private String jobTitle;
	private List<JobSpeakerVo> jobSpeakers;
	private List<JobScriptVo> jobScripts;
	
	public static JobContentVo of(JobM entity, List<JobSpeakerVo> speakers, List<JobScriptVo> scripts) {
        return JobContentVo.builder()
        		.id(entity.getId())
        		.jobId(entity.getJobId())
        		.jobTitle(entity.getJobTitle())
        		.jobSpeakers(speakers)
        		.jobScripts(scripts)
        		.build();
    }
}
