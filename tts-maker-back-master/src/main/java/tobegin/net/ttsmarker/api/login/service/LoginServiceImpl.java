package tobegin.net.ttsmarker.api.login.service;

import org.hibernate.hql.internal.ast.tree.IntoClause;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tobegin.net.ttsmarker.api.login.domain.User;
import tobegin.net.ttsmarker.api.login.repository.UserRepository;
import tobegin.net.ttsmarker.api.login.vo.UserVo;

import javax.jws.soap.SOAPBinding;
import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserRepository userRepository;

    public User userCheck(String id, String pw){

        User user = userRepository.searchUser(id, pw);

        if(user == null){
            return null;
        }
        return user;
    }

    public boolean idCheck(String id){

        if(userRepository.getUserId(id) == null){
            return true;
        }else{
            return false;
        }
    }

    public void registUser(User user){
        user.setConfirm(0);
        user.setAuth(2);
        try {
            userRepository.save(user);
        }catch(Exception e){
            System.out.print(e);
        }
    }

    public List<User> waitingUser(){
        List<User> userList = userRepository.waitingUser();
        return userList;
    }

    public String registConfirm(User user){
        try{
            user.setConfirm(2);
            userRepository.save(user);
        }catch(Exception e){
            System.out.print(e);
            return "FAIL";
        }

        return "SUCCESS";
    }

    public String registReject(User user) {

        try {
            User userInfo = userRepository.getUserInfo(user.getId());
            userRepository.delete(userInfo);
        } catch (Exception e) {
            e.printStackTrace();
            ;
            return "FAIL";
        }
        return "SUCCESS";
    }

}
