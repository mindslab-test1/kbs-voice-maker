package tobegin.net.ttsmarker.api.job.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tobegin.net.ttsmarker.api.job.domain.JobSpeaker;

public interface JobSpeakerRepository extends JpaRepository<JobSpeaker, Long> { 
	
	@Query("select p from JobSpeaker p where p.isDeleted = 'N' and p.jobId = ?1")
	List<JobSpeaker> getActiveSpeakerListFindByJobId(String jobId, Sort sort);
	
	@Query("select p from JobSpeaker p where p.jobId = ?1 and p.speaker.speakerId = ?2")
	Optional<JobSpeaker> getJobSpeakerFindByJobIdAndSpeakerId(String jobId, String speakerId);
}
