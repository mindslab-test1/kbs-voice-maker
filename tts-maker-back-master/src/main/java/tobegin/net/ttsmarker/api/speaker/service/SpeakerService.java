package tobegin.net.ttsmarker.api.speaker.service;

import java.util.List;

import tobegin.net.ttsmarker.api.speaker.domain.Speaker;
import tobegin.net.ttsmarker.api.speaker.vo.SpeakerVo;

public interface SpeakerService {
	
	/**
	 * <pre>
	 * 스피커(화자) 전체 리스트 조회
	 * </pre>
	 * @return
	 */
	public List<SpeakerVo> getAllList();
	
	
	/**
	 * <pre>
	 * 스피커(화자) 전체 모델 엔티 조회
	 * </pre>
	 * @return
	 */
	public List<Speaker> allEntities();


	/**
	 * <pre>
	 * 스피커(화자) 랜덤 조회
	 * </pre>
	 * @return
	 */
	public SpeakerVo getRandomSpeaker();
}
