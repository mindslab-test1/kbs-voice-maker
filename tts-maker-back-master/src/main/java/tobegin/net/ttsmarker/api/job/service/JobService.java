package tobegin.net.ttsmarker.api.job.service;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import tobegin.net.ttsmarker.api.job.vo.request.JobContentSaveVo;
import tobegin.net.ttsmarker.api.job.vo.request.JobScriptSaveVo;
import tobegin.net.ttsmarker.api.job.vo.response.JobContentVo;
import tobegin.net.ttsmarker.api.job.vo.response.JobListVo;
import tobegin.net.ttsmarker.api.job.vo.response.JobScriptVo;
import tobegin.net.ttsmarker.api.job.vo.response.JobSpeakerVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface JobService {
	/**
	 * <pre>
	 * JOB List 조회
	 * </pre>
	 * @return
	 */
	public List<JobListVo> getJobList();

	/**
	 * <pre>
	 * 각 User ID마다 JOB List 조회
	 * </pre>
	 *
	 * @return
	 */
	public List<JobListVo> getJobListByUserId(String userId);
	
	/**
	 * <pre>
	 * JOB ID별 선택한 화자 및 스크립트 로드
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public JobContentVo getJobContent(String jobId);
	
	/**
	 * <pre>
	 * Job ID별 선택한 스피커(화자) List 조회
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public List<JobSpeakerVo> getJobSpeakerList(String jobId);
	
	/**
	 * <pre>
	 * Job ID별 스크립트 LIST 조회
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public List<JobScriptVo> getJobScriptList(String jobId);
	
	/**
	 * <pre>
	 * script 조회(Job ID + Script ID)
	 * </pre>
	 * @param jobId
	 * @param scriptId
	 * @return
	 */
	public JobScriptVo getJobScript(String jobId, String scriptId);

	/**
	 * <pre>
	 * 스크립트(JobScriptSaveVo) 저장(Redis)
	 * </pre>
	 * @param jobId
	 * @param scriptId
	 * @param saveVo
	 * @return
	 */
	public boolean setJobScriptsSaveVoTagetByRedis(String jobId, String scriptId, JobScriptSaveVo saveVo);
	
	/**
	 * <pre>
	 * 선택한 화자 List(JobSpeakerSaveVo) 저장(Redis)
	 * </pre>
	 * @param jobId
	 * @param jobSpeakerIdList
	 * @return
	 */
	public boolean setJobSpeakerSaveVosTagetByRedis(String jobId, List<String> jobSpeakerIdList);

	/**
	 * <pre>
	 * JOB 저장
	 * </pre>
	 * @param jobId
	 * @param jobContentSaveVo
	 * @return
	 */
	public boolean saveAsJobContent(String jobId, JobContentSaveVo jobContentSaveVo);

    /**
     * <pre>
     * 해상 openApi 정보 가져옴
     * </pre>
     *
     * @return
     */
    public String getSeaFcst();

	// Download
	public byte[] downloadEachTTS(JobContentSaveVo jobContentSaveVo) throws IOException;
	public void downloadTTS(JobContentSaveVo jobContentSaveVo, HttpServletResponse response) throws IOException;
	// Send
	public Object sendTTStoDMZ(JobContentSaveVo jobContentSaveVo) throws IOException;
	public Object checkItemCode(JobContentSaveVo jobContentSaveVo);
}
