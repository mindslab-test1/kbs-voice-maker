package tobegin.net.ttsmarker.api.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tobegin.net.ttsmarker.api.login.domain.User;
import tobegin.net.ttsmarker.api.login.service.LoginService;
import tobegin.net.ttsmarker.api.login.vo.UserVo;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    LoginService loginService;

    @ResponseBody
    @PostMapping(value="/userCheck")
    public User userCheck(@RequestBody UserVo userVo) throws IOException{
        return loginService.userCheck(userVo.getUserId(), userVo.getUserPw());
    }

    @ResponseBody
    @PostMapping(value="/idCheck")
    public boolean idCheck(@RequestBody UserVo userVo) throws IOException{
        return loginService.idCheck(userVo.getUserId());
    }

    @ResponseBody
    @PostMapping(value="/regist")
    public void registUser(@RequestBody User user) throws  IOException{
        loginService.registUser(user);
    }

    @ResponseBody
    @PostMapping(value="/waitingUser")
    public List<User> waitingUser() throws IOException{
        return loginService.waitingUser();
    }

    @ResponseBody
    @PostMapping(value="/registConfirm")
    public String registConfirm(@RequestBody User user) throws IOException{

        return loginService.registConfirm(user);
    }

    @ResponseBody
    @PostMapping(value = "/registReject")
    public String registReject(@RequestBody User user) throws IOException {

        return loginService.registReject(user);
    }
}
