package tobegin.net.ttsmarker.api.provider.maumai.tts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClientException;

public class MaumAIApiException extends WebClientException {
	private static final long serialVersionUID = 7544085780421806822L;
	
	private final HttpStatus statusCode;
	private final String errorCode;
	private final String errorMessage;
    
    public MaumAIApiException(HttpStatus statusCode, String errorCode, String errorMessage) {
		super(statusCode.getReasonPhrase());
        this.statusCode = statusCode;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}