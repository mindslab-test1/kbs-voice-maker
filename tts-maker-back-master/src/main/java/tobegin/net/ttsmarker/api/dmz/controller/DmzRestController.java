package tobegin.net.ttsmarker.api.dmz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tobegin.net.ttsmarker.api.dmz.service.DmzService;
import tobegin.net.ttsmarker.api.dmz.vo.request.DmzUserIdVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/dmz")
public class DmzRestController {

    @Autowired
    private DmzService dmzService;

    @ResponseBody
    @PostMapping(value = "/userCategoryList")
    public Object userCategoryList(@RequestBody DmzUserIdVo dmzUserIdVo, HttpServletRequest request, HttpServletResponse response) {
        System.out.println(dmzService.getUserCategoryList(dmzUserIdVo.getUserId()));
        return dmzService.getUserCategoryList(dmzUserIdVo.getUserId());
    }
}
