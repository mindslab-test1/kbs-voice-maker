package tobegin.net.ttsmarker.api.speaker.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tobegin.net.ttsmarker.api.speaker.domain.Speaker;

public interface SpeakerRepository extends JpaRepository<Speaker, Long> { 
	
	@Query("select p from Speaker p where p.isDeleted = 'N'")
	List<Speaker> findActiveList();
}
