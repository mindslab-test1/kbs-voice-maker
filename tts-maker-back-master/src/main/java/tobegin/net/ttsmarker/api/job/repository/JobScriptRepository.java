package tobegin.net.ttsmarker.api.job.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tobegin.net.ttsmarker.api.job.domain.JobScript;

public interface JobScriptRepository extends JpaRepository<JobScript, Long> { 
	
	@Query("select p from JobScript p where p.isDeleted = 'N' and p.jobId = ?1")
	List<JobScript> getActiveScriptListFindByJobId(String jobId, Sort sort);
	
	@Query("select p from JobScript p where p.isDeleted = 'N' and p.jobId = ?1 and p.scriptId = ?2")
	JobScript getJobScriptFindByJobIdAndScriptId(String jobId, String scriptId);
}
