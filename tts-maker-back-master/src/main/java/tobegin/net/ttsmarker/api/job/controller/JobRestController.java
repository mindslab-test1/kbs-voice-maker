package tobegin.net.ttsmarker.api.job.controller;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.common.io.Files;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;
import tobegin.net.ttsmarker.api.job.service.JobService;
import tobegin.net.ttsmarker.api.job.vo.request.JobContentSaveVo;
import tobegin.net.ttsmarker.api.job.vo.request.JobScriptSaveVo;
import tobegin.net.ttsmarker.api.job.vo.response.JobContentVo;
import tobegin.net.ttsmarker.api.job.vo.response.JobListVo;
import tobegin.net.ttsmarker.api.job.vo.response.JobScriptVo;
import tobegin.net.ttsmarker.api.job.vo.response.JobSpeakerVo;
import tobegin.net.ttsmarker.api.log.domain.Log;
import tobegin.net.ttsmarker.api.log.service.LogService;
import tobegin.net.ttsmarker.api.login.domain.User;
import tobegin.net.ttsmarker.api.login.repository.UserRepository;
import tobegin.net.ttsmarker.api.script.domain.dto.response.TTSMakeResultVo;
import tobegin.net.ttsmarker.api.script.service.TTSMakeService;
import tobegin.net.ttsmarker.util.FfmpegUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping("/job")
public class JobRestController {

	@Value("${KBS_OPENAPI}")
	private String KBS_OPENAPI;

	@Value("${DMZ_SERVER}")
	private String DMZ_SERVER;

	@Value("${OPEN_API}")
	private String OPEN_API;

	@Autowired
	private JobService jobService;
	
	@Autowired
	private TTSMakeService ttsService;

	@ResponseBody
	@GetMapping(value = "/list")
	public List<JobListVo> list() throws IOException {
		return jobService.getJobList();
	}

	@ResponseBody
	@GetMapping(value = "/listByUserId/{userId}")
	public List<JobListVo> listByUserId(@PathVariable String userId) throws IOException {
		return jobService.getJobListByUserId(userId);
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}")
	public JobContentVo getJobContent(@PathVariable String jobId) throws IOException {
		return jobService.getJobContent(jobId);
	}
	
	@ResponseBody
	@PutMapping(value = "/{jobId}")
	public boolean saveAsJobContent(@PathVariable String jobId, @RequestBody JobContentSaveVo jobContentSaveVo) {
		return jobService.saveAsJobContent(jobId, jobContentSaveVo);
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}/speaker")
	public List<JobSpeakerVo> jobSpeakerList(@PathVariable String jobId) throws IOException {
		return jobService.getJobSpeakerList(jobId);
	}
		
	@ResponseBody
	@PutMapping(value = "/{jobId}/speaker")
	public boolean setJobSpeakerSaveVosTagetByRedis(@PathVariable String jobId, @RequestBody List<String> jobSpeakerIdList) {
		return jobService.setJobSpeakerSaveVosTagetByRedis(jobId, jobSpeakerIdList);
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}/script")
	public List<JobScriptVo> jobScriptList(@PathVariable String jobId) throws IOException {
		return jobService.getJobScriptList(jobId);
	}
	
	@ResponseBody
	@PutMapping(value = "/{jobId}/script/{scriptId}")
	public boolean setJobScriptsSaveVoTagetByRedis(@PathVariable String jobId, @PathVariable String scriptId, @RequestBody JobScriptSaveVo saveVo) {
		return jobService.setJobScriptsSaveVoTagetByRedis(jobId, scriptId, saveVo);
	}
	
	@ResponseBody
	@PostMapping(value = "/toMakeTTS/{voiceName}")
	public Mono<TTSMakeResultVo> requestTTSMakeFile(@PathVariable String voiceName, @RequestParam String text, @RequestParam(required = false) String dummyFileName) throws IOException, InterruptedException {
		System.out.println("requestTTSMakeFile : "+voiceName);
		return ttsService.requestTTSMakeFile(voiceName, text, dummyFileName);
	}

	@ResponseBody
	@PostMapping(value = "/sendTTS")
	public Object sendTTStoDMZ(@RequestBody JobContentSaveVo jobContentSaveVo) throws IOException {
		return jobService.sendTTStoDMZ(jobContentSaveVo);
	}

	@ResponseBody
	@PostMapping(value = "/checkItemCode")
	public Object checkItemCode(@RequestBody JobContentSaveVo jobContentSaveVo) {
		return jobService.checkItemCode(jobContentSaveVo);
	}

	@ResponseBody
	@PostMapping(value = "/downloadTTS")
	public void downloadTTS(@RequestBody JobContentSaveVo jobContentSaveVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
		jobService.downloadTTS(jobContentSaveVo, response);
	}

	@ResponseBody
	@PostMapping(value = "/downloadEachTTS")
	public byte[] downloadEachTTS(@RequestBody JobContentSaveVo jobContentSaveVo, HttpServletRequest request, HttpServletResponse response) throws IOException, URISyntaxException {
		return jobService.downloadEachTTS(jobContentSaveVo);
	}


    @ResponseBody
    @GetMapping(value = "/meteorological")
    public String getMeteorologicalApi() {

        return jobService.getSeaFcst();
    }
}
