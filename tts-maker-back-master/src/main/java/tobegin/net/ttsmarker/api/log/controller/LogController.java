package tobegin.net.ttsmarker.api.log.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tobegin.net.ttsmarker.api.log.domain.Log;
import tobegin.net.ttsmarker.api.log.service.LogService;
import tobegin.net.ttsmarker.api.log.vo.LogVo;

import java.util.List;

@RestController
@RequestMapping("/log")
public class LogController {

    @Autowired
    LogService logService;

    @ResponseBody
    @GetMapping(value = "/getAllLog")
    public List<Log> getAllLog() {

        return logService.getAllLog();
    }

    @ResponseBody
    @PostMapping(value = "/searchLog")
    public List<Log> getSearchLog(@RequestBody LogVo logVo) {

        System.out.println("search : " + logVo.getSearch());

        return logService.searchLog(logVo.getSearch());
    }
}
