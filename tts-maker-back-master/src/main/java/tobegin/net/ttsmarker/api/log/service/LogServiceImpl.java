package tobegin.net.ttsmarker.api.log.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tobegin.net.ttsmarker.api.log.domain.Log;
import tobegin.net.ttsmarker.api.log.repository.LogRepository;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    public void saveLog(Log log){
        try {
            logRepository.save(log);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public List<Log> getAllLog() {
        return logRepository.findAll();
    }

    public List<Log> searchLog(String search) {

        return logRepository.searchLog(search);
    }

}
