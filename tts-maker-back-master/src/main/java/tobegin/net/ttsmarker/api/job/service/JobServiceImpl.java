package tobegin.net.ttsmarker.api.job.service;

import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import com.google.common.io.Files;
import com.google.gson.Gson;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import tobegin.net.ttsmarker.api.Fcst.domain.LandFcstItem;
import tobegin.net.ttsmarker.api.Fcst.domain.SeaFcstItem;
import tobegin.net.ttsmarker.api.Fcst.service.FcstService;
import tobegin.net.ttsmarker.api.Fcst.service.FcstServiceImpl;
import tobegin.net.ttsmarker.api.exception.TTSMarkerApiException;
import tobegin.net.ttsmarker.api.job.domain.JobM;
import tobegin.net.ttsmarker.api.job.domain.JobScript;
import tobegin.net.ttsmarker.api.job.domain.JobSpeaker;
import tobegin.net.ttsmarker.api.job.repository.JobMRepository;
import tobegin.net.ttsmarker.api.job.repository.JobScriptRepository;
import tobegin.net.ttsmarker.api.job.repository.JobSpeakerRepository;
import tobegin.net.ttsmarker.api.job.vo.request.JobContentSaveVo;
import tobegin.net.ttsmarker.api.job.vo.request.JobScriptSaveVo;
import tobegin.net.ttsmarker.api.job.vo.request.JobSpeakerSaveVo;
import tobegin.net.ttsmarker.api.job.vo.response.*;
import tobegin.net.ttsmarker.api.log.service.LogService;
import tobegin.net.ttsmarker.api.login.repository.UserRepository;
import tobegin.net.ttsmarker.api.log.domain.Log;
import tobegin.net.ttsmarker.api.login.domain.User;
import tobegin.net.ttsmarker.api.script.service.TTSMakeService;
import tobegin.net.ttsmarker.api.speaker.domain.Speaker;
import tobegin.net.ttsmarker.api.speaker.service.SpeakerService;
import tobegin.net.ttsmarker.util.DateUtils;

import com.google.gson.reflect.TypeToken;
import tobegin.net.ttsmarker.util.FfmpegUtils;

@Service
public class JobServiceImpl implements JobService {
	Logger logger = Logger.getLogger(JobServiceImpl.class.getName());

    @Value("${WEATHER_API}")
    private String WEATHER_API;

    @Value("${WEATHER_API_KEY}")
    private String WEATHER_API_KEY;

	@Value("${DMZ_SERVER}")
	private String DMZ_SERVER;

	@Autowired
	private JobMRepository jobMRepository;

	@Autowired
	private JobSpeakerRepository jobSpeakerRepository;

	@Autowired
	private JobScriptRepository jobScriptRepository;

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@Autowired
	private SpeakerService speakerService;

	@Autowired
	private TTSMakeService ttsService;

	@Autowired
	private LogService logService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private FcstServiceImpl fcstService;

	@PersistenceContext
	private EntityManager em;

    private String current = "";

	@Override
	public List<JobListVo> getJobList() {
		try {
			List<JobM> list = jobMRepository.getActiveJobList(Sort.by("orderSeq"));
			return list.stream().map(JobListVo::of).collect(Collectors.toList());
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0001", "작 리스트 조회 중 오류가 발생했습니다.");
		}
	}

	@Override
	public List<JobListVo> getJobListByUserId(String userId) {
		try {
			List<JobM> list = jobMRepository.getActiveJobListByUserId(userId, Sort.by("orderSeq"));
			return list.stream().map(JobListVo::of).collect(Collectors.toList());
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0001", "작 리스트 조회 중 오류가 발생했습니다.");
		}
	}

	@Override
	public JobContentVo getJobContent(String jobId) {
		try {
			JobM jobM = jobMRepository.getJobContentFindByJobId(jobId);
			List<JobSpeakerVo> jobSpeakerVoList = Lists.newArrayList();
			List<JobScriptVo> jobScriptVoList = Lists.newArrayList();
			if (jobM != null) {
				List<JobSpeaker> jobSpeakers = jobM.getJobSpeakers();
				if (!CollectionUtils.isEmpty(jobSpeakers)) {
					jobSpeakerVoList = makeJobSpeakerVoList(jobSpeakers);
					setJobSpeakerSaveVoMapTagetByRedis(jobId, makeJobSpeakerSaveVoMap(jobSpeakers));
				}
				List<JobScript> jobScripts = jobM.getJobScripts();
				if (!CollectionUtils.isEmpty(jobScripts)) {
					jobScriptVoList = makeJobScriptVoList(jobScripts);
					setJobScriptsSaveVoMapTagetByRedis(jobId, makeJobScriptSaveVoMap(jobScripts));
				}
				return JobContentVo.of(jobM, jobSpeakerVoList, jobScriptVoList);
			}
			return new JobContentVo();
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0002", "작업 데이터를 불러오는중  오류가 발생했습니다.");
		}
	}

	@Override
	public List<JobSpeakerVo> getJobSpeakerList(String jobId) {
		try {
			List<JobSpeaker> list = jobSpeakerRepository.getActiveSpeakerListFindByJobId(jobId, Sort.by("orderSeq"));
			return list.stream().map(JobSpeakerVo::of).collect(Collectors.toList());
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0003", "화자 데이터를 불러오는중 오류가 발생했습니다.");
		}
	}

	@Override
	public List<JobScriptVo> getJobScriptList(String jobId) {
		try {
			List<JobScript> list = jobScriptRepository.getActiveScriptListFindByJobId(jobId, Sort.by("orderSeq"));
			return list.stream().map(JobScriptVo::of).collect(Collectors.toList());
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0004", "Script List 데이터를 불러오는중 오류가 발생했습니다.");
		}
	}

	@Override
	public JobScriptVo getJobScript(String jobId, String scriptId) {
		try {
			return JobScriptVo.of(jobScriptRepository.getJobScriptFindByJobIdAndScriptId(jobId, scriptId));
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0005", "Script 데이터를 불러오는중 오류가 발생했습니다.");
		}
	}

	/**
	 * <pre>
	 * JobSpeakerSaveVoMap 저장(Redis), JobContent 조회시 저장
	 * </pre>
	 * @param jobId
	 * @param jobSpeakerSaveVoMap
	 * @return
	 */
	private boolean setJobSpeakerSaveVoMapTagetByRedis(String jobId, Map<String, JobSpeakerSaveVo> jobSpeakerSaveVoMap) {
		try {
			String key = String.format("%s:speakers", jobId);
			redisTemplate.opsForHash().putAll(key, jobSpeakerSaveVoMap);
			redisTemplate.expireAt(key, DateUtils.asDate(LocalDateTime.now().plusDays(2)));
			return true;
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0006", "JobSpeakerSaveVo Map 업데이트 처리 중 오류가 발생했습니다.");
		}
	}

	/**
	 * <pre>
	 * JobSpeakerSaveVo 조회(Redis), JobContent 저장시 사용
	 * </pre>
	 * @param jobId
	 * @param speakerId
	 * @return
	 */
	public JobSpeakerSaveVo getJobSpeakerSaveVoFromRedis(String jobId, String speakerId) {
		try {
			String key = String.format("%s:speakers", jobId);
			JobSpeakerSaveVo saveSpeaker  = (JobSpeakerSaveVo) redisTemplate.opsForHash().get(key, speakerId);
			return saveSpeaker;
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0007", "JobSpeakerSaveVo 조회중 오류가 발생했습니다.");
		}
	}

	/**
	 * <pre>
	 * jobScriptsSaveVoMap 저장(Redis), JobContent 조회시 저장
	 * </pre>
	 * @param jobId
	 * @param jobScriptsSaveVoMap
	 * @return
	 */
	public boolean setJobScriptsSaveVoMapTagetByRedis(String jobId, Map<String, JobScriptSaveVo> jobScriptsSaveVoMap) {
		try {
			String key = String.format("%s:scripts", jobId);
			redisTemplate.opsForHash().putAll(key, jobScriptsSaveVoMap);
			redisTemplate.expireAt(key, DateUtils.asDate(LocalDateTime.now().plusDays(2)));
			return true;
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0008", "JobScriptSaveVo Map 업데이트 처리 중 오류가 발생했습니다.");
		}
	}



	/**
	 * <pre>
	 * JobScriptSaveVo 저장(Redis)
	 * </pre>
	 * @param jobId
	 * @param scriptId
	 * @param saveVo
	 * @return
	 */
	@Override
	public boolean setJobScriptsSaveVoTagetByRedis(String jobId, String scriptId, JobScriptSaveVo saveVo) {
		try {
			String key = String.format("%s:scripts", jobId);
			redisTemplate.opsForHash().put(key, scriptId, saveVo);
			redisTemplate.expireAt(key, DateUtils.asDate(LocalDateTime.now().plusDays(2)));
			return true;
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0009", "JobScriptSaveVo 업데이트 처리 중 오류가 발생했습니다.");
		}
	}

	/**
	 * <pre>
	 * JobSpeakerSaveVo List 저장(Redis)
	 * </pre>
	 * @param jobId
	 * @param jobSpeakerIdList
	 * @return
	 */
	@Override
	public boolean setJobSpeakerSaveVosTagetByRedis(String jobId, List<String> jobSpeakerIdList) {
		try {
			Map<String, JobSpeakerSaveVo> jobScriptsSaveVoMap = new HashMap<String, JobSpeakerSaveVo>();
			if(!CollectionUtils.isEmpty(jobSpeakerIdList))
			{
				List<JobSpeakerSaveVo> jobSpeakerSaveVoList = jobSpeakerIdList.stream().map(jobSpeakerId->JobSpeakerSaveVo.builder().jobId(jobId).speakerId(jobSpeakerId).build()).collect(Collectors.toList());
				jobScriptsSaveVoMap = jobSpeakerSaveVoList.stream().collect(Collectors.toMap(JobSpeakerSaveVo::getSpeakerId, Function.identity()));
			}
			setJobSpeakerSaveVoMapTagetByRedis(jobId, jobScriptsSaveVoMap);
			return false;
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0009", "JobScriptSaveVo 업데이트 처리 중 오류가 발생했습니다.");
		}
	}

	/**
	 * <pre>
	 * JobScriptSaveVo 조회(Redis), JobContent 저장시 사용
	 * </pre>
	 * @param jobId
	 * @param scriptId
	 * @return
	 */
	public JobScriptSaveVo getJobScriptSaveVoFromRedis(String jobId, String scriptId) {
		try {
			String key = String.format("%s:scripts", jobId);
			JobScriptSaveVo saveScript  = (JobScriptSaveVo) redisTemplate.opsForHash().get(key, scriptId);
		return saveScript;
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ00010", "JobScriptSaveVo 조회중 오류가 발생했습니다.");
		}
	}

	@Transactional
	@Override
	public boolean saveAsJobContent(String jobId, JobContentSaveVo jobContentSaveVo) {

        //기존에 작업한 것이 있는 경우
		JobM jobM_check = jobMRepository.getJobTitle(jobContentSaveVo.getJobTitle(), jobContentSaveVo.getUserId());
        if (jobM_check != null) {
            jobId = jobM_check.getJobId();
        }

		try {
			List<JobSpeaker> jobSpeakers = Lists.newArrayList();
			List<JobScript> jobScripts = Lists.newArrayList();
			// JOB 레코드 저장
			JobM jobM = jobMRepository.getJobContentFindByJobId(jobId);
			if (jobM != null) {
				jobM.setJobTitle(jobContentSaveVo.getJobTitle());
				em.merge(jobM);

				jobSpeakers = jobM.getJobSpeakers();
				jobScripts = jobM.getJobScripts();
			} else {
				jobM = new JobM();
				jobM.setJobId(jobId);
				jobM.setUserId(jobContentSaveVo.getUserId());
				jobM.setJobTitle(jobContentSaveVo.getJobTitle());
				em.merge(jobM);
			}
			Map<String, JobSpeaker> jobSpeakersMap = makeJobSpeakerMap(jobSpeakers);
			Map<String, Speaker> speakersMap = speakerService.allEntities().stream()
					.collect(Collectors.toMap(Speaker::getSpeakerId, Function.identity()));

			// 선택한 화자 삭제
			if (!CollectionUtils.isEmpty(jobContentSaveVo.getDeletedSpeakers())) {
				for (JobSpeakerSaveVo deletedSpeaker : jobContentSaveVo.getDeletedSpeakers()) {
					if (jobSpeakersMap.containsKey(deletedSpeaker.getSpeakerId())) {
						JobSpeaker jobSpeaker = jobSpeakersMap.get(deletedSpeaker.getSpeakerId());
						jobSpeaker.setIsDeleted("Y");
						em.merge(jobSpeaker);
					}
				}
			}
			// 선택한 화자 저장/수정
			if (!CollectionUtils.isEmpty(jobContentSaveVo.getSpeakers())) {
				long orderSeq = 1L;
				for (JobSpeakerSaveVo saveSpeaker : jobContentSaveVo.getSpeakers()) {

					boolean existDbJobSpeaker = jobSpeakersMap.containsKey(saveSpeaker.getSpeakerId());
					JobSpeakerSaveVo jobSpeakerTempData = getJobSpeakerSaveVoFromRedis(jobId, saveSpeaker.getSpeakerId());
					saveSpeaker = Optional.ofNullable(jobSpeakerTempData).orElse(saveSpeaker); //비어있으면 saveSpeaker 안비어있으면 jobSpeakerTempData

					if (existDbJobSpeaker) {
						JobSpeaker jobSpeaker = jobSpeakersMap.get(saveSpeaker.getSpeakerId());
						jobSpeaker.setOrderSeq(orderSeq);
						jobSpeaker.setUpdatedAt(LocalDateTime.now());
						em.merge(jobSpeaker);
					} else {
						JobSpeaker jobSpeaker = new JobSpeaker();
						jobSpeaker.setJobId(jobId);
						jobSpeaker.setSpeaker(speakersMap.get(saveSpeaker.getSpeakerId()));
						jobSpeaker.setOrderSeq(orderSeq);
						jobSpeaker.setCreatedAt(LocalDateTime.now());
						jobSpeaker.setUpdatedAt(LocalDateTime.now());
						em.merge(jobSpeaker);
					}
					orderSeq++;
				}
			}

			Map<String, JobScript> jobScriptsMap = makeJobScriptMap(jobScripts);

			// 기존 스크립트 삭제
			if (!CollectionUtils.isEmpty(jobContentSaveVo.getDeletedScripts())) {
				for (JobScriptSaveVo deletedScript : jobContentSaveVo.getDeletedScripts()) {
                    if (jobScriptsMap.containsKey(deletedScript.getScriptId())) {
						JobScript jobScript = jobScriptsMap.get(deletedScript.getScriptId());
						jobScript.setIsDeleted("Y");
						em.merge(jobScript);
					}
				}
			}
			// 스크립트 저장/수정
			if (!CollectionUtils.isEmpty(jobContentSaveVo.getScripts())) {
				long orderSeq = 1L;
				for (JobScriptSaveVo saveScript : jobContentSaveVo.getScripts()) {
					boolean existDbJobScript = jobScriptsMap.containsKey(saveScript.getScriptId());

					JobScriptSaveVo jobScriptTemp = getJobScriptSaveVoFromRedis(jobId, saveScript.getScriptId());
					saveScript = Optional.ofNullable(jobScriptTemp).orElse(saveScript);
					Speaker speaker = speakersMap.get(saveScript.getSpeakerId());
					JobScript jobScript = null;
					if (existDbJobScript) {
                        jobScript = jobScriptsMap.get(saveScript.getScriptId());
						jobScript = adjUpdateJobScript(existDbJobScript, jobScript, speaker, saveScript, orderSeq);
					} else {
						jobScript = new JobScript();
						jobScript.setJobId(jobId);
						jobScript = adjUpdateJobScript(existDbJobScript, jobScript, speaker, saveScript, orderSeq);
					}
					em.merge(jobScript);
					setJobScriptsSaveVoTagetByRedis(jobId, saveScript.getScriptId(), JobScriptSaveVo.of(jobScript));
					orderSeq++;
				}
			}

			em.flush();
			return true;
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0007", "JOB 저장 처리 중 오류가 발생했습니다. : " + ex.getMessage());
		}
	}

	/**
	 * <pre>
	 * JobScript 데이터 가공(신규/업데이트)
	 * </pre>
	 * @param existJobScript
	 * @param jobScript
	 * @param speaker
	 * @param saveVo
	 * @param orderSeq
	 * @return
	 */
	private JobScript adjUpdateJobScript(boolean existJobScript, JobScript jobScript, Speaker speaker, JobScriptSaveVo saveVo, long orderSeq) {
		JobSettingsVo settings = Optional.ofNullable(saveVo.getSettings()).orElse(new JobSettingsVo());
		if (existJobScript) {
			jobScript.setScriptText(saveVo.getScriptText());								// TTS 스크립트 Text
			jobScript.setTtsResourceUrl(saveVo.getTtsResourceUrl());					// TTS Resource URL정보
			jobScript.setTtsResourceExpiryDate(saveVo.getTtsResourceExpiryDate()); 	// TTS Resource 만료일
			jobScript.setSettingsStyle(settings.getStyle());								// 음성 제어 스타일 정보
			jobScript.setSettingsVolume(settings.getVolume()); 							// 음성 제어 볼륨 정보
			jobScript.setSettingsSpeed(settings.getSpeed());								// 음성 제어 스피드 정보
			jobScript.setSettingsSleep(settings.getSleep());								// 음성 제어 읽기 쉬기 정보
			jobScript.setOrderSeq(orderSeq);												// 정렬순서
			jobScript.setTtsState(saveVo.getTtsState());									// TTS 상태
			jobScript.setRowState("S");														// ROW 상태
			jobScript.setCreatedAt(LocalDateTime.now());
			jobScript.setUpdatedAt(LocalDateTime.now());
		} else {
			jobScript.setScriptId(saveVo.getScriptId());
			jobScript.setScriptText(saveVo.getScriptText());								// TTS 스크립트 Text
			jobScript.setSpeaker(speaker);													// 화자 정보
			jobScript.setTtsResourceUrl(saveVo.getTtsResourceUrl());					// TTS Resource URL정보
			jobScript.setTtsResourceExpiryDate(saveVo.getTtsResourceExpiryDate()); 	// TTS Resource 만료일
			jobScript.setSettingsStyle(settings.getStyle());								// 음성 제어 스타일 정보
			jobScript.setSettingsVolume(settings.getVolume()); 							// 음성 제어 볼륨 정보
			jobScript.setSettingsSpeed(settings.getSpeed());								// 음성 제어 스피드 정보
			jobScript.setSettingsSleep(settings.getSleep());								// 음성 제어 읽기 쉬기 정보
			jobScript.setOrderSeq(orderSeq);												// 정렬순서
			jobScript.setTtsState(saveVo.getTtsState());									// TTS 상태
			jobScript.setRowState("S");														// ROW 상태
			jobScript.setCreatedAt(LocalDateTime.now());
		}
		return jobScript;
	}

	/**
	 * <pre>
	 * List<JobSpeaker> to Map<String, JobSpeaker> 처리
	 * </pre>
	 * @param jobSpeakers
	 * @return
	 */
	private Map<String, JobSpeaker> makeJobSpeakerMap(List<JobSpeaker> jobSpeakers) {
		if (!CollectionUtils.isEmpty(jobSpeakers)) {
			Function<JobSpeaker, String> jobSpeakerIdFunction = (JobSpeaker JobSpeaker) -> JobSpeaker.getSpeaker().getSpeakerId();
			return jobSpeakers.stream().collect(Collectors.toMap(jobSpeakerIdFunction, Function.identity()));
		}
		return new HashMap<>();
	}

	/**
	 * <pre>
	 * List<JobScript> to Map<String, JobScript> 처리
	 * </pre>
	 * @param jobScripts
	 * @return
	 */
	private Map<String, JobScript> makeJobScriptMap(List<JobScript> jobScripts) {
		if (!CollectionUtils.isEmpty(jobScripts)) {
			return jobScripts.stream().collect(Collectors.toMap(JobScript::getScriptId, Function.identity()));
		}
		return new HashMap<>();
	}

	/**
	 * <pre>
	 * List<jobSpeakers> to List<JobSpeakerVo> 처리
	 * </pre>
	 * @param jobSpeakers
	 * @return
	 */
	private List<JobSpeakerVo> makeJobSpeakerVoList(List<JobSpeaker> jobSpeakers) {
		if (!CollectionUtils.isEmpty(jobSpeakers)) {
			return jobSpeakers.stream().map(JobSpeakerVo::of).collect(Collectors.toList());
		}
		return Lists.newArrayList();
	}

	/**
	 * <pre>
	 * List<JobScript> to List<JobScriptVo> 처리
	 * </pre>
	 * @param jobScripts
	 * @return
	 */
	private List<JobScriptVo> makeJobScriptVoList(List<JobScript> jobScripts) {
		if (!CollectionUtils.isEmpty(jobScripts)) {
			return jobScripts.stream().map(JobScriptVo::of).collect(Collectors.toList());
		}
		return Lists.newArrayList();
	}

	/**
	 * <pre>
	 * List<JobScript> to Map<String, JobScriptSaveVo> 처리
	 * </pre>
	 * @param jobScripts
	 * @return
	 */
	private Map<String, JobScriptSaveVo> makeJobScriptSaveVoMap(List<JobScript> jobScripts) {
		if (!CollectionUtils.isEmpty(jobScripts)) {
			return jobScripts.stream().map(JobScriptSaveVo::of).collect(Collectors.toMap(JobScriptSaveVo::getScriptId, Function.identity()));
		}
		return new HashMap<>();
	}

	/**
	 * <pre>
	 * List<JobSpeaker> to Map<String, JobSpeakerSaveVo> 처리
	 * </pre>
	 * @param jobSpeakers
	 * @return
	 */
	private Map<String, JobSpeakerSaveVo> makeJobSpeakerSaveVoMap(List<JobSpeaker> jobSpeakers) {
		if (!CollectionUtils.isEmpty(jobSpeakers)) {
			return jobSpeakers.stream().map(JobSpeakerSaveVo::of).collect(Collectors.toMap(JobSpeakerSaveVo::getSpeakerId, Function.identity()));
		}
		return new HashMap<>();
	}

    /**
     * <pre>
     * 해상 openApi 정보 가져옴
     * </pre>
     *
     * @return
     */
    public String getSeaFcst() {
        return fcstService.getSeaFcst();
    }

    // Download
	@Override
	public void downloadTTS(JobContentSaveVo jobContentSaveVo, HttpServletResponse response) throws IOException {
		// Getting VoiceName & Text list
		ArrayList<String> urlArray = new ArrayList<>();
		for(JobScriptSaveVo script: jobContentSaveVo.getScripts()) {
			String url = ttsService.requestTTSMakeFile(script.getVoiceName(), script.getScriptText(), null).block().getDownloadURL();
			urlArray.add(url);
		}

		File tempDir = Files.createTempDir();
		String outputPath = tempDir.getPath() +"/"+jobContentSaveVo.getJobTitle()+".wav";
		System.out.println(outputPath);
		try {
			String ffmpegCommand = FfmpegUtils.ffmpegCommandGenerator(urlArray, outputPath);
			Process p1 = Runtime.getRuntime().exec(ffmpegCommand);
//			String ffmpegCommand2 = FfmpegUtils.ffmpegScaling(outputPath);
//			Process p2 = Runtime.getRuntime().exec(ffmpegCommand2);
			p1.waitFor();

//			p2.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}


		// Download File
		File varFile = new File(outputPath);
		if(varFile.exists()){
			response.setContentType("audio/x-wav");
			response.addHeader("Content-Disposition", "attachment; filename="+varFile.getName());
			try{
				Files.copy(varFile, response.getOutputStream());
				response.getOutputStream().flush();
			}catch (Exception e) {
				// TODO => Logger
				e.printStackTrace();
			}
		}

		Log log = new Log();
		log.setUserId(jobContentSaveVo.getUserId());
		User userInfo = userRepository.getUserbyUserId(jobContentSaveVo.getUserId());
		log.setName(userInfo.getName());
		log.setCode("Download");
		log.setContent("Download: " + jobContentSaveVo.getJobTitle());
		logService.saveLog(log);

		// Deleting temp directory
		FileUtils.cleanDirectory(tempDir);
		tempDir.delete();
	}
	@Override
	public byte[] downloadEachTTS(JobContentSaveVo jobContentSaveVo) throws IOException {
		// Getting VoiceName & Text list
		ArrayList<String> urlArray = new ArrayList<>();
		for(JobScriptSaveVo script: jobContentSaveVo.getScripts()) {
			String url = ttsService.requestTTSMakeFile(script.getVoiceName(), script.getScriptText(), null).block().getDownloadURL();
			urlArray.add(url);
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedOutputStream bos = new BufferedOutputStream(baos);
		ZipArchiveOutputStream zos = new ZipArchiveOutputStream(bos);

		Integer line = 0;
		for(String url: urlArray) {
			line++;
			zos.setEncoding("UTF-8");
			zos.putArchiveEntry(new ZipArchiveEntry("line-" + line + ".wav"));

			HttpGet req = new HttpGet(url);
			CloseableHttpClient httpClient = HttpClients.createDefault();
			HttpResponse res = httpClient.execute(req);
			org.apache.http.HttpEntity entity = res.getEntity();
			InputStream is = entity.getContent();

			IOUtils.copy(is, zos);

			is.close();
			zos.closeArchiveEntry();
		}
		if(zos!= null){
			zos.finish();
			zos.flush();
			IOUtils.closeQuietly(zos);
		}

		Log log = new Log();
		log.setUserId(jobContentSaveVo.getUserId());
		User userInfo = userRepository.getUserbyUserId(jobContentSaveVo.getUserId());
		log.setName(userInfo.getName());
		log.setCode("Download-Zip");
		log.setContent("Download-zip: " + jobContentSaveVo.getJobTitle());
		logService.saveLog(log);

		IOUtils.closeQuietly(bos);
		IOUtils.closeQuietly(baos);
		return baos.toByteArray();
	}
	// Send
	@Override
	public Object sendTTStoDMZ(JobContentSaveVo jobContentSaveVo) throws IOException {
    	System.out.println("sendTTStoDMZ");
    	System.out.println(jobContentSaveVo);
		// Getting VoiceName & Text list
		ArrayList<String> urlArray = new ArrayList<>();
		for(JobScriptSaveVo script: jobContentSaveVo.getScripts()) {
			String url = ttsService.requestTTSMakeFile(script.getVoiceName(), script.getScriptText(), null).block().getDownloadURL();
			urlArray.add(url);
		}

		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmss");
		String currentTime = format1.format(System.currentTimeMillis());

		File tempDir = Files.createTempDir();
		String filename = jobContentSaveVo.getJobTitle()+"-" + currentTime + ".wav";
		String outputPath2 = tempDir.getPath() + "/" + "firstWav.wav";
		String outputPath = tempDir.getPath() + "/" + filename;

		try {
			String ffmpegCommand = FfmpegUtils.ffmpegCommandGenerator(urlArray, outputPath2);
			Process p1 = Runtime.getRuntime().exec(ffmpegCommand);
			p1.waitFor();
		} catch (Exception e) {
			e.printStackTrace();
		}

		// Sending merged wav file to DMZ
		File wavFile = new File(outputPath2);
		long wavFileDuration = 0;
		try {
			wavFileDuration = (long)getDuration(wavFile);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		}
		try{
			String ffmpegCommand = FfmpegUtils.ffmpeg24bit(outputPath2, outputPath);
			Process p1 = Runtime.getRuntime().exec(ffmpegCommand);
			System.out.println("this is 24bit wait for " + p1.waitFor());
		}catch (Exception e){
			e.printStackTrace();
		}

//		File wavFile2 = new File(outputPath);

		File wavFile2 = new  File(new String(outputPath.getBytes(), "UTF-8"));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("file", new FileSystemResource(wavFile2));
		body.add("systemId", "AI");
		body.add("daletUserId", jobContentSaveVo.getUserId());
		body.add("programTitle", jobContentSaveVo.getDestination().getTitle());
		body.add("categoryId", jobContentSaveVo.getDestination().getCategoryId());
		body.add("itemCode", jobContentSaveVo.getDestination().getItemCode());
		body.add("channel", jobContentSaveVo.getDestination().getChannel());
		body.add("fileName", filename);
		body.add("durationMS", wavFileDuration);

		logger.info(body);
		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Object> response = restTemplate.postForEntity(DMZ_SERVER+"/tts/createProgram", requestEntity, Object.class);
		System.out.println(response);
		Log log = new Log();
		log.setUserId(jobContentSaveVo.getUserId());
		User userInfo = userRepository.getUserbyUserId(jobContentSaveVo.getUserId());
		log.setName(userInfo.getName());
		log.setCode(jobContentSaveVo.getDestination().getKind());
		log.setContent("Send: " + jobContentSaveVo.getJobTitle());
		logService.saveLog(log);

		// Deleting temp directory
		FileUtils.cleanDirectory(tempDir);
		tempDir.delete();

		return response;
	}

	@Override
	public Object checkItemCode(JobContentSaveVo jobContentSaveVo) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
		body.add("itemCode", jobContentSaveVo.getDestination().getItemCode());
		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Object> response = restTemplate.postForEntity(DMZ_SERVER+"/tts/checkItemCode", requestEntity, Object.class);

		System.out.println("ChekcItemCode");
		System.out.println(response);
		System.out.println();
		return response;
	}

	private static float getDuration(File file) throws IOException, UnsupportedAudioFileException {

		AudioInputStream ais = AudioSystem.getAudioInputStream(file);
		AudioFormat format = ais.getFormat();
		long audioFileLength = file.length();
		int frameSize = format.getFrameSize();
		float frameRate = format.getFrameRate();
		float durationInMS = (audioFileLength/ (frameSize * frameRate)) * 1000;

		System.out.println(durationInMS);
		return (durationInMS);
	}
}
