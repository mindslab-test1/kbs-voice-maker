package tobegin.net.ttsmarker.api.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tobegin.net.ttsmarker.api.login.domain.User;
import tobegin.net.ttsmarker.api.login.vo.UserVo;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u from User u where u.userId = ?1 and u.userPw = ?2")
    User searchUser(String id, String pw);

    @Query("select u.userId from User u where u.userId = ?1")
    String getUserId(String id);

    @Query("select u from User u where u.confirm = 0")
    List<User> waitingUser();

    @Query("select u from User u where u.id = ?1")
    User getUserInfo(String id);


    @Query("select u from User u where u.userId = ?1")
    User getUserbyUserId(String userId);
}
