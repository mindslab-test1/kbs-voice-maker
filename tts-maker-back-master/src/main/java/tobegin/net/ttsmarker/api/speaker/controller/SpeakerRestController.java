package tobegin.net.ttsmarker.api.speaker.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import tobegin.net.ttsmarker.api.speaker.service.SpeakerService;
import tobegin.net.ttsmarker.api.speaker.vo.SpeakerVo;

@RestController
@RequestMapping("/speaker")
@CrossOrigin
public class SpeakerRestController {
	
	@Autowired
	private SpeakerService speakerService;
	
	@ResponseBody
	@GetMapping
	public List<SpeakerVo> list() throws IOException {
		return speakerService.getAllList();
	}
	
	@ResponseBody
	@GetMapping(value = "/randomSpeaker")
	public SpeakerVo randomSpeaker() throws IOException {
		System.out.println("randomSpeaker start...");
		return speakerService.getRandomSpeaker();
	}
}
