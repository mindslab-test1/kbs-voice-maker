package tobegin.net.ttsmarker.api.log.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import tobegin.net.ttsmarker.api.log.domain.Log;

import java.util.List;

public interface LogRepository extends JpaRepository<Log, Long> {
    @Query("select l from Log l where l.userId = :search or l.code = :search or l.name = :search or l.content like concat('%',:search, '%')")
    List<Log> searchLog(String search);
}
