package tobegin.net.ttsmarker.api.job.vo.request;

import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import tobegin.net.ttsmarker.api.job.domain.JobSpeaker;
import tobegin.net.ttsmarker.api.speaker.domain.Speaker;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobSpeakerSaveVo {
	private String jobId;		// JOB ID
	private String speakerId;	// 화자 ID
	
	@JsonIgnore
	public static JobSpeakerSaveVo of(JobSpeaker entity) {
		Speaker speaker = Optional.ofNullable(entity.getSpeaker()).orElse(new Speaker());
		return JobSpeakerSaveVo.builder()
        		.jobId(entity.getJobId())
        		.speakerId(speaker.getSpeakerId())
        		.build();
    }
}
