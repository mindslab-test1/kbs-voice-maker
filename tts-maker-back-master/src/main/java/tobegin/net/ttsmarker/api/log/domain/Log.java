package tobegin.net.ttsmarker.api.log.domain;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.io.Serializable;
import java.util.Date;
@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "LOG_T")
public class Log implements Serializable {
    //관리자번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private String id;

    //user ID
    @Column(name = "USER_ID", columnDefinition = "varchar(100)")
    private String userId;

    //이름
    @Column(name = "NAME", columnDefinition = "varchar(100)")
    private String name;
    //비즈니스 코드
    @Column(name = "CODE", columnDefinition = "varchar(100)")
    private String code;
    //내용
    @Column(name = "CONTENT", columnDefinition = "varchar(100)")
    private String content;
    //등록일시
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INS_DT")
    private Date createdAt;
}
