package tobegin.net.ttsmarker.api.login.service;

import tobegin.net.ttsmarker.api.login.domain.User;
import tobegin.net.ttsmarker.api.login.vo.UserVo;

import java.util.List;

public interface LoginService {

    public User userCheck(String id, String pw);

    public boolean idCheck(String id);

    public void registUser(User user);

    public List<User> waitingUser();

    public String registConfirm(User user);

    public String registReject(User user);
}
