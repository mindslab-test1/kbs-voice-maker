package tobegin.net.ttsmarker.api.login.vo;

import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserVo {
    private String userId;
    private String userPw;
    private String name;
}
