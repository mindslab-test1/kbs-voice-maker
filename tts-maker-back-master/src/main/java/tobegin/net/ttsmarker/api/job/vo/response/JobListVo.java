package tobegin.net.ttsmarker.api.job.vo.response;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import tobegin.net.ttsmarker.api.job.domain.JobM;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobListVo {
	private long id;
	private String jobId;
	private String jobTitle;
	private long orderSeq;
	private LocalDateTime updatedAt;
	
	public static JobListVo of(JobM entity) {
        return JobListVo.builder()
        		.id(entity.getId())
        		.jobId(entity.getJobId())
        		.jobTitle(entity.getJobTitle())
        		.orderSeq(entity.getOrderSeq())
        		.updatedAt(entity.getUpdatedAt())
        		.build();
    }
}
