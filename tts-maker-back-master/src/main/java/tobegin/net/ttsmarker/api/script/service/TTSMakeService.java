package tobegin.net.ttsmarker.api.script.service;

import reactor.core.publisher.Mono;
import tobegin.net.ttsmarker.api.script.domain.dto.response.TTSMakeResultVo;

public interface TTSMakeService {
	public Mono<TTSMakeResultVo> requestTTSMakeFile(String voiceName, String text, String dummyFileName);
	public Mono<TTSMakeResultVo> fakeRequestTTSMakeFile(String voiceName, String text, String dummyFileName);
}
