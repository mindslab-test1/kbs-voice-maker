package tobegin.net.ttsmarker.api.speaker.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import tobegin.net.ttsmarker.api.exception.TTSMarkerApiException;
import tobegin.net.ttsmarker.api.speaker.domain.Speaker;
import tobegin.net.ttsmarker.api.speaker.repository.SpeakerRepository;
import tobegin.net.ttsmarker.api.speaker.vo.SpeakerVo;
import tobegin.net.ttsmarker.util.DateUtils;

@Service
public class SpeakerServiceImpl implements SpeakerService {
	
	@Autowired
	private SpeakerRepository speakerRepository;
	
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	@Override
	public List<SpeakerVo> getAllList() {
		try {
			List<Speaker> list = speakerRepository.findActiveList(); 
			return list.stream().map(SpeakerVo::of).collect(Collectors.toList());
		}catch(Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0001", "화자 리스트 조회중 오류가 발생했습니다.");
		}
	}
	
	public List<Speaker> allEntities() {
		try {
			return speakerRepository.findActiveList();
		}catch(Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0002", "화자 리스트 조회중 오류가 발생했습니다.");
		}
	}

	@Override
	public SpeakerVo getRandomSpeaker() {
		System.out.println("getRandomSpeaker start ...");
		try {
			SpeakerVo speakerVo = getRandomSpeakerTagetByRedis();
			if(speakerVo == null) {
				System.out.println("getRandomSpeaker : speakerVo is null ");
				List<Speaker> allEntities = speakerRepository.findActiveList();
				int count = allEntities.size();
				System.out.println(count);
				int rnd = new Random().nextInt(count);
				speakerVo = SpeakerVo.of(allEntities.get(rnd));
				setRandomSpeakerTagetByRedis(speakerVo);
			}


			return speakerVo;
		}catch(TTSMarkerApiException ex) {
			throw ex;
		}
		catch(Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0003", "랜덤 화자 조회중 오류가 발생했습니다.");
		}
	}
	
	/**
	 * <pre>
	 * 기본 화자 SpeakerVo 저장(Redis)
	 * </pre>
	 * @param speakerVo
	 */
	private void setRandomSpeakerTagetByRedis(SpeakerVo speakerVo) {
		try {
			String key = "default:randomSpeaker";
			redisTemplate.opsForValue().set(key, speakerVo);
			redisTemplate.expireAt(key, DateUtils.asDate(LocalDateTime.now().plusDays(1)));
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0004", "JobScriptSaveVo 업데이트 처리중 오류가 발생했습니다.");
		}
	}

	/**
	 * <pre>
	 * 기본 화자 SpeakerVo 불러오기(Redis)
	 * </pre>
	 * @return
	 */
	private SpeakerVo getRandomSpeakerTagetByRedis() {
		System.out.println("getRandomSpeakerTagetByRedis start...");
		try {
			String key = "default:randomSpeaker";
			//System.out.println((SpeakerVo) redisTemplate.opsForValue());
			return (SpeakerVo) redisTemplate.opsForValue().get(key);
		} catch (Exception ex) {
			throw new TTSMarkerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0005", "JobScriptSaveVo 업데이트 처리중 오류가 발생했습니다.");
		}
	}
}
