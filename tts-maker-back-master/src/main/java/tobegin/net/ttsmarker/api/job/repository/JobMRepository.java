package tobegin.net.ttsmarker.api.job.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.google.common.base.Optional;

import tobegin.net.ttsmarker.api.job.domain.JobM;

@Repository
public interface JobMRepository extends JpaRepository<JobM, Long> { 
	
	@Query("select p from JobM p where p.isDeleted = 'N'")
	List<JobM> getActiveJobList(Sort sort);

	@Query("select p from JobM p where p.isDeleted = 'N' and p.userId = ?1")
	List<JobM> getActiveJobListByUserId(String userId, Sort sort);
	
	@EntityGraph(attributePaths = {"jobSpeakers"}, type = EntityGraphType.LOAD)  
	@Query("select p from JobM p where p.isDeleted = 'N' and p.jobId = ?1")
	JobM getJobContentFindByJobId(String jobId);
	
	@Query("select p from JobM p where p.isDeleted = 'N' and p.jobId = ?1")
	Optional<JobM> getJobFindByJobId(String jobId);

    @Query("select p from JobM p where p.jobTitle = ?1 and p.userId = ?2")
    JobM getJobTitle(String jobTitle, String userId);
}
