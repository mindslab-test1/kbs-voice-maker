package tobegin.net.ttsmarker.api.provider.maumai.tts.vo;

import lombok.Data;

@Data
public class MaumAIResponseMessage {
	private String message;
	private int status;
}
