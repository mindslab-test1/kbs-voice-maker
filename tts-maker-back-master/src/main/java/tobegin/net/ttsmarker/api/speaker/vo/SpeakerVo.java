package tobegin.net.ttsmarker.api.speaker.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import tobegin.net.ttsmarker.api.speaker.domain.Speaker;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SpeakerVo {
	private String speakerId; // 화자 관리 번호
	private String speakerNm; // 화자 이름
	private String voiceName; // TTS VoiceName
	private String voiceSampleUrl; // TTS Voice 샘플 URL
	private String imgUrl;    // 이미지 URL
	private String speakerDesc; // 화자 설명
	
	public static SpeakerVo of(Speaker entity) {
        return SpeakerVo.builder()
        		.speakerId(entity.getSpeakerId())
                .speakerNm(entity.getSpeakerNm())
                .voiceName(entity.getVoiceName())
                .voiceSampleUrl(entity.getVoiceSampleUrl())
                .imgUrl(entity.getImgUrl())
                .speakerDesc(entity.getSpeakerDesc())
                .build();
    }
}
