package tobegin.net.ttsmarker.api.log.service;
import tobegin.net.ttsmarker.api.log.domain.Log;
import tobegin.net.ttsmarker.api.log.domain.Log;

import java.util.List;

public interface LogService {
    public void saveLog(Log log);

    public List<Log> getAllLog();

    public List<Log> searchLog(String search);
}
