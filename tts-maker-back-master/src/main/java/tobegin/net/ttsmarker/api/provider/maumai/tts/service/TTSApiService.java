package tobegin.net.ttsmarker.api.provider.maumai.tts.service;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;
import tobegin.net.ttsmarker.api.provider.maumai.tts.exception.MaumAIApiException;
import tobegin.net.ttsmarker.api.provider.maumai.tts.vo.MaumAIRequestParam;
import tobegin.net.ttsmarker.api.provider.maumai.tts.vo.MaumAIResponseFile;

import java.util.UUID;

@Service
public class TTSApiService {
	
	private static final String API_BASE_URL = "https://kbs-file.maum.ai";
	private static final String API_MIME_TYPE = "application/json";
	private static final String CACHE_CONTROL = "no-cache";
	private static final String API_ID = "digitalp.kbs";
	private static final String API_KEY = "f1ba5fe6e536467f97ddda50041fa16b";

    private final WebClient webClient;
    
	public TTSApiService() {
		this.webClient = WebClient.builder()
                .baseUrl(API_BASE_URL)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, API_MIME_TYPE)
                .defaultHeader(HttpHeaders.CACHE_CONTROL, CACHE_CONTROL)
                /*.filter(ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
                    if (clientResponse.statusCode().isError()) {
                        return clientResponse.bodyToMono(MaumAIResponseFile.class)
                                .flatMap(errorDetails -> Mono.error(new MaumAIApiException(clientResponse.statusCode(), errorDetails.getErrorCode())));
                    }
                    return Mono.just(clientResponse);
                }))*/
                .build();
  
	}
	/*
	public ExchangeFilterFunction errorHandlingFilter() {
        return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
        	HttpStatus statusCode = clientResponse.statusCode();
            if(statusCode!=null && (statusCode.is5xxServerError() || statusCode.is4xxClientError()) ) {
                 return clientResponse.bodyToMono(MaumAIResponseFile.class)
                         .map(errorBody -> {
                        	 return Mono.error(new MaumAIResponseFile());
                      });
            }else {
                return Mono.just(clientResponse);
            }
        });
    }
	*/
	
	//https://stackoverflow.com/questions/57652631/how-do-custom-exception-in-webflux-webclient
	//https://stackoverflow.com/questions/49485523/get-api-response-error-message-using-web-client-mono-in-spring-boot
	//https://supawer0728.github.io/2018/03/11/Spring-request-model3/
	//https://www.baeldung.com/exception-handling-for-rest-with-spring
	public Mono<MaumAIResponseFile> requestTTSMakeFile(String voiceName, String text) {
        Float speed = 1.0f;
		UUID fileName = UUID.randomUUID();
		MaumAIRequestParam param = new MaumAIRequestParam(API_ID, API_KEY, voiceName, text, speed, fileName.toString());

		return webClient.post().uri("/makeFile")
				.body(Mono.just(param), MaumAIRequestParam.class).retrieve()
				.onStatus(HttpStatus::isError,  response -> {
					MaumAIApiException exception = null;
					HttpStatus statusCode = response.statusCode();
					if(statusCode != null) {
						switch(statusCode) {
							case FORBIDDEN:
								exception = new MaumAIApiException(response.statusCode(), "ERRA0001", "MaumAI API provider 인증 오류");
								break;
							case BAD_GATEWAY:
								exception = new MaumAIApiException(response.statusCode(), "ERRA0002", "MaumAI API provider와의 통신 오류");
								break;
							case BAD_REQUEST:
								exception = new MaumAIApiException(response.statusCode(), "ERRA0003", "The request parameter value is invalid.(MaumAI API)");
								break;
							default:
								if(statusCode.is4xxClientError() || statusCode.is5xxServerError()) {
									exception = new MaumAIApiException(response.statusCode(), "ERRA0000", "MaumAIAPI provider 통신 오류");
								}
								break;
						}
					}else {
						exception = new MaumAIApiException(response.statusCode(), "ERRA0000", "MaumAIAPI provider 통신 오류");
					}
					return Mono.error(exception);
				})
                .bodyToMono(MaumAIResponseFile.class);
	}
}
