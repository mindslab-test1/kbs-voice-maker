package tobegin.net.ttsmarker.api.speaker.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "SPEAKER", uniqueConstraints = {@UniqueConstraint(name = "UK_SPEAKER_ID", columnNames = { "SPEAKER_ID" }) })
public class Speaker implements Serializable {
	
	private static final long serialVersionUID = -5368605104228648343L;

	//관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private long id;
    
    //화자 ID
    @Column(name = "SPEAKER_ID", columnDefinition = "varchar(36)")
    private String speakerId;
    
    //화자 이름
    @Column(name = "SPEAKER_NM", columnDefinition = "varchar(100)")
    private String speakerNm;
    
    //TTS 보이스 이름
    @Column(name = "VOICE_NAME", columnDefinition = "varchar(100)")
    private String voiceName;
    
    //TTS 보이스 Sample URL
    @Column(name = "VOICE_SAMPLE_URL", columnDefinition = "varchar(1000)")
    private String voiceSampleUrl;
    
    //화자 이미지
    @Column(name = "IMG_URL", columnDefinition = "varchar(255)")
    private String imgUrl;
    
    //설명
    @Column(name = "SPEAKER_DESC", columnDefinition = "varchar(255)")
    private String speakerDesc;
    
    //삭제여부
    @Column(name = "DEL_YN", columnDefinition = "char(1) default 'N'")
    private String isDeleted;
    
    //등록일시
    @Column(name = "INS_DT")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    //수정일시
    @Column(name = "UPD_DT")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}
