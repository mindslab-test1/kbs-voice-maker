package tobegin.net.ttsmarker.api.script.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import tobegin.net.ttsmarker.api.job.service.JobService;
import tobegin.net.ttsmarker.api.job.vo.response.JobScriptVo;

@RestController
@RequestMapping("/script")
public class ScriptRestController {
		
	@Autowired
	private JobService jobService; 
	
	@ResponseBody
	@GetMapping(value = "/{jobId}/list")
	public List<JobScriptVo> getJobScriptList(@PathVariable String jobId) {
		return jobService.getJobScriptList(jobId);
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}/{scriptId}")
	public JobScriptVo getJobScript(@PathVariable String jobId, @PathVariable String scriptId) {
		return jobService.getJobScript(jobId, scriptId);
	}
}
