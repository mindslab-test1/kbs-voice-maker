package tobegin.net.ttsmarker.configurers;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = { "tobegin.net.ttsmarker.api" }, // TODO Repository 패키지 지정
        transactionManagerRef = "ttsMakerTransactionManager",
        entityManagerFactoryRef = "ttsMakerEntityManagerFactory"
)
public class DataSourceConfiguration {
	
	@Bean(name = "ttsMakerDataSource")
	@Primary
    @ConfigurationProperties("spring.datasource.hikari")
    public DataSource mariaDataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }

    @Primary
    @Bean(name = "ttsMakerEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder, @Qualifier("ttsMakerDataSource") DataSource dataSource) {
        return builder.dataSource(dataSource).packages("tobegin.net.ttsmarker.api.job.domain", "tobegin.net.ttsmarker.api.speaker.domain", "tobegin.net.ttsmarker.api.login.domain", "tobegin.net.ttsmarker.api.log.domain").build();
    }

    @Primary
    @Bean(name = "ttsMakerTransactionManager")
    public PlatformTransactionManager transactionManager(@Qualifier("ttsMakerEntityManagerFactory") EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
