package tobegin.net.ttsmarker.configurers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
		List<String> permitAllList = new ArrayList<String>();
		permitAllList.add("/**");
		permitAllList.add("/static/**");
		permitAllList.add("/maumAi/**");
		permitAllList.add("/script/**");
		permitAllList.add("/tts/**");
		permitAllList.add("/speaker/**");
		
		http.cors().and()
			.csrf().disable()
			/*.csrf().and().addFilterAfter(new CsrfCookieGeneratorFilter(), CsrfFilter.class)
			.exceptionHandling().and()*/
			.authorizeRequests()
		 	.antMatchers(permitAllList.toArray(new String[] {})).permitAll()
		 	.anyRequest().authenticated();
			
	}

	@Bean(name = "corsConfigurationSource")
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOrigins(Arrays.asList("https://kbs-tool.maum.ai","http://kbs-tool.maum.ai","http://15.165.16.245","https://15.165.16.245","http://15.165.16.245:8002","https://15.165.16.245:8002","http://114.108.173.114:8002", "https://localhost:8002", "http://127.0.0.1:8002", "http://10.52.180.37:8000", "http://localhost:8002", "https://tts.tobegin.net", "https://www.tobegin.net:8888"));
		configuration.setAllowedMethods(Arrays.asList("GET", "POST", "OPTIONS", "DELETE", "PUT"));
		configuration.setAllowedHeaders(Arrays.asList("Content-Type", "content-type", "x-requested-with", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "x-auth-token", "x-app-id", "Origin","Accept", "X-Requested-With", "Access-Control-Request-Method", "Access-Control-Request-Headers"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}

}
